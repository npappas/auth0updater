const wretch = require('wretch');
const fetch = require('@turist/fetch').default(require('node-fetch'));

wretch().polyfills({
  fetch,
});

const auth0Endpoint = process.env.AUTH0_ENDPOINT;
const auth0ClientId = process.env.API_CLIENT_ID;
const auth0ClientSecret = process.env.API_CLIENT_SECRET;
const spaClientId = process.env.SPA_CLIENT_ID;

const updateType = process.argv[2] === 'add' ? 'add' : 'del';
const urlToChange = process.argv[3];

if (!urlToChange) {
  console.error('no url specified');
  process.exit(1);
}

console.log(`Running an ${updateType} operation for ${urlToChange}`);

const main = async () => {
  const token = await wretch(`${auth0Endpoint}/oauth/token`)
    .post({
      client_id: auth0ClientId,
      client_secret: auth0ClientSecret,
      audience: `${auth0Endpoint}/api/v2/`,
      grant_type: 'client_credentials',
    })
    .json();

  const clientInfo = await wretch(
    `${auth0Endpoint}/api/v2/clients/${spaClientId}`,
  )
    .auth(`Bearer ${token.access_token}`)
    .get()
    .json();

  const {
    callbacks,
    allowed_logout_urls,
    allowed_origins,
    web_origins,
  } = clientInfo;

  const patch = {
    callbacks: modifyArray(callbacks, updateType, urlToChange),
    allowed_logout_urls: modifyArray(
      allowed_logout_urls,
      updateType,
      urlToChange,
    ),
    allowed_origins: modifyArray(allowed_origins, updateType, urlToChange),
    web_origins: modifyArray(web_origins, updateType, urlToChange),
  };

  await wretch(`${auth0Endpoint}/api/v2/clients/${spaClientId}`)
    .auth(`Bearer ${token.access_token}`)
    .patch(patch)
    .json();

  console.log('Done');
  process.exit(0);
};

const modifyArray = (array, updateType, url) => {
  if (updateType === 'add') {
    if (!array.includes(url)) {
      array.push(url);
      return array;
    }
  } else if (updateType === 'del') {
    if (array.includes(url)) {
      return array.filter((item) => item !== url);
    }
  }

  return array;
};

main();
