FROM node:13-alpine

WORKDIR /app

COPY package*.json .

RUN npm ci

COPY index.js .
USER node

CMD [ "node", "index.js" ]
